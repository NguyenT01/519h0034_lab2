import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Login me',
      home: Scaffold(
        body: LoginScreen(),
      ),
    );
  }
}

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }
}

class LoginState extends State<StatefulWidget> {
  final formKey = GlobalKey<FormState>();
  late String email;
  late String password;
  late String fname;
  late String lname;
  late String name;
  late String birth;
  late String address;
  TextEditingController dateInput = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    dateInput.text = "";

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        margin: EdgeInsets.all(20.0),
        child: Form(
          key: formKey,
          child: Column(
            children: [
              emailField(),
              Container(
                margin: EdgeInsets.only(top: 20.0),
              ),
              Row(
                children: [
                  Expanded(
                    child: nameField(),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    child: nameField2(),
                  ),
                ],
              ),
              const SizedBox(
                height: 40,
              ),
              brithField(),
              const SizedBox(
                height: 40,
              ),
              addressField(),
              const SizedBox(
                height: 40,
              ),
              loginButton()
            ],
          ),
        ),
      ),
    );
  }

  Widget emailField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration:
          InputDecoration(icon: Icon(Icons.person), labelText: 'Email address'),
      validator: (value) {
        if (!value!.contains('@')) {
          return "Please input valid email.";
        }

        return null;
      },
      onSaved: (value) {
        email = value as String;
      },
    );
  }

  Widget nameField() {
    return TextFormField(
      //obscureText: true,
      decoration: InputDecoration(
          icon: Icon(Icons.accessibility),
          labelText: 'Last Name and Middle Name'),
      validator: (value) {
        if (value!.contains(new RegExp(r'[0-9]'))) {
          return "Name don't have a number.";
        }

        return null;
      },
      onSaved: (value) {
        fname = value as String;
      },
    );
  }

  Widget nameField2() {
    return TextFormField(
      //obscureText: true,
      decoration: InputDecoration(
          icon: Icon(Icons.contact_page), labelText: 'Your Name'),
      validator: (value) {
        if (value!.contains(new RegExp(r'[0-9]'))) {
          return "Name don't have a number.";
        }

        return null;
      },
      onSaved: (value) {
        name = value as String;
      },
    );
  }

  Widget brithField() {
    return Row(children: [
      Expanded(
        child: TextFormField(
          //obscureText: true,
          decoration: InputDecoration(
              icon: Icon(Icons.calendar_today), labelText: 'Calendar'),
          validator: (value) {
            if (true) {
              return null;
            }

            return null;
          },
          onSaved: (value) {
            birth = value as String;
          },
          readOnly: true,
          controller: dateInput,
        ),
        flex: 3,
      ),
      Expanded(
        child: ElevatedButton(
          child: Text('SELECT'),
          onPressed: () async {
            var currentDate = DateTime.now();
            final dated =  await showDatePicker(
                context: context,

                initialDate: currentDate,
                firstDate: DateTime(currentDate.year-100),
                lastDate: DateTime.now());

            setState(() {
              if(dated!=null){
                birth = DateFormat('dd-MM-yyyy').format(dated);
                dateInput.text = birth;
              }
            });

          },
        ),
        flex: 1,
      )
    ]);
  }

//        showDatePicker(context: context, initialDate: DateTime.now(), firstDate: DateTime.now(), lastDate: DateTime(2099));
  Widget addressField() {
    return TextFormField(
      //obscureText: true,
      decoration: InputDecoration(icon: Icon(Icons.map), labelText: 'Address'),
      validator: (value) {
        if (value ==null) {
          return "This filed should not empty";
        }

        return null;
      },
      onSaved: (value) {
        address = value as String;
      },
    );
  }

  Widget loginButton() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            formKey.currentState!.save();
            showToast(context);
          }
        },
        child: Text('Login'));
  }
  
  void showToast(BuildContext context)
  {
    final sf = ScaffoldMessenger.of(context);
    sf.showSnackBar(
      SnackBar(content: const Text('Login successful'), action: SnackBarAction(label: 'UNDO', onPressed: sf.hideCurrentSnackBar),)
    );
  }
}
