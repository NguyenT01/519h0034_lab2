import 'package:flutter/material.dart';
import 'dart:async';

class DictStream {
  int length = 0;
  StreamController _counterController = StreamController<int>.broadcast();
  Stream get lengthStream => _counterController.stream;

  void takeText(String text){
    length = text.length;
    _counterController.sink.add(length);
    print(text.toLowerCase());
  }

  void dispose()
  {
    _counterController.close();
  }
}