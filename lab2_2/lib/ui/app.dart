import 'package:flutter/material.dart';
import '../stream/dict_stream.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stream',
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('Stream'),
        ),
        body: HomePage(),
      ),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final formKey = GlobalKey<FormState>();
  DictStream dictStream = DictStream();

  @override
  void dispose() {
    // TODO: implement dispose
    dictStream.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Center(
      child: Form(
        key: formKey,
        child: Column(
          children: [
            const SizedBox(
              height: 16,
            ),
            textField(),
            const SizedBox(
              height: 16,
            ),
            textResult(),
          ],
        ),
      ),
    ));
  }

  Widget textField() {
    return TextFormField(
      decoration: InputDecoration(
        hintText: 'Text here',
      ),
      onChanged: (value) {
        dictStream.takeText(value);
      },
    );
  }

  Widget textResult() {
    return StreamBuilder(
      stream: dictStream.lengthStream,
      builder: (context, snapshot)=>Text(
        snapshot.hasData ? snapshot.data.toString() : "0",
        style: TextStyle(fontSize: 28),
      ),
    );
  }
}
