import 'package:flutter/material.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Extra exercise',
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar:
            AppBar(title: Text('Extra exercise'), backgroundColor: Colors.red),
        body: InputApp(),
      ),
    );
  }
}

class InputApp extends StatefulWidget {
  const InputApp({Key? key}) : super(key: key);

  @override
  State<InputApp> createState() => _InputAppState();
}

class _InputAppState extends State<InputApp> {
  final formKey = GlobalKey<FormState>();
  late String email;
  late String ho;
  late String lot;
  late String ten;
  late String birthDay;
  String country= "Vietnam";
  late String city;
  late String district;
  List<String> listCountry = ["Vietnam", "Thailand"];
  Map<String, List<String>> cities = {
    "Default": [],
    "Vietnam": ["Hochiminh", "Hanoi"],
    "Thailand": ["Bangkok", "Aduthaya"],
  };
  Map<String, List<String>> districts = {
    "Default": [],
    "Hochiminh": ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
    "Hanoi": ["Dong Anh", "Linh Xuan", "Dong Da"],
    "Bangkok": ["1", "2", "3", "4", "5"],
    "Aduthaya": ["6", "7", "8", "9", "10"],
  };

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        padding: EdgeInsets.all(16),
        child: Form(
          key: formKey,
          child: Column(
            children: [
              emailForm(),
              const SizedBox(
                height: 16,
              ),
              Row(
                children: [
                  Expanded(
                    child: nameField(),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    child: nameField2(),
                  ),
                ],
              ),
              const SizedBox(
                height: 16,
              ),
              nameField3(),
              const SizedBox(
                height: 16,
              ),
              countryDropDown(),
              const SizedBox(
                height: 16,
              ),
              submitBtn(),
            ],
          ),
        ),
      ),
    );
  }

  Widget emailForm() {
    return TextFormField(
      decoration: InputDecoration(
        icon: Icon(Icons.email),
        hintText: 'Your email',
      ),
      keyboardType: TextInputType.emailAddress,
      validator: (text) {
        if (text == null) {
          return 'Please type your email';
        } else if (!text.contains('@')) {
          return "Email format doesn't match";
        }
        return null;
      },
      onSaved: (text) {
        email = text!;
      },
    );
  }

  Widget submitBtn() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            formKey.currentState!.save();
          }
        },
        child: Text(
          "SUBMIT",
          style: TextStyle(fontWeight: FontWeight.bold),
        ));
  }

  Widget nameField() {
    return TextFormField(
      //obscureText: true,
      decoration: InputDecoration(
          icon: Icon(Icons.accessibility), labelText: 'Firstname'),
      validator: (value) {
        if (value == null) {
          return "Please fill in this field";
        } else if (value.contains(new RegExp(r'[0-9]'))) {
          return "Name don't have a number.";
        }
        return null;
      },
      onSaved: (value) {
        ho = value as String;
      },
    );
  }

  Widget nameField2() {
    return TextFormField(
      //obscureText: true,
      decoration: InputDecoration(
          icon: Icon(Icons.contact_page), labelText: 'Middle Name'),
      validator: (value) {
        if (value == null) {
          return "Please fill in this field";
        } else if (value.contains(new RegExp(r'[0-9]'))) {
          return "Name don't have a number.";
        }

        return null;
      },
      onSaved: (value) {
        lot = value as String;
      },
    );
  }

  Widget nameField3() {
    return TextFormField(
      //obscureText: true,
      decoration:
          InputDecoration(icon: Icon(Icons.contact_page), labelText: 'Name'),
      validator: (value) {
        if (value == null) {
          return "Please fill in this field";
        } else if (value.contains(new RegExp(r'[0-9]'))) {
          return "Name don't have a number.";
        }

        return null;
      },
      onSaved: (value) {
        ten = value as String;
      },
    );
  }

  Widget countryDropDown() {
    return DropdownButton(
    items: listCountry.map<DropdownMenuItem<String>>((String value){
      return DropdownMenuItem<String>(
        value:value,
        child: Text(value),
      );
    }).toList(),
    onChanged: (String? newValue){
      setState(() {
        country = newValue!;
      });
    },
    value: country,
    elevation: 16,
      isExpanded: true,
    );
  }
}
